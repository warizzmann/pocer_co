<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_m extends CI_Model {
    
    function get_detail($url_kategori)
    {
        $query = $this->db->where('url_kategori', $url_kategori)->limit(1)->get('kategori_artikel');
        return $query->row_array();
    }
    
    function get_all_where($kategori_artikel_id, $limit = null, $start = null, $order_by = 'artikel_id')
    {
        $this->db->limit($limit, $start);
        $query = $this->db->order_by($order_by,'desc')->join('penulis', 'penulis.penulis_id = artikel.penulis_id')->join('kategori_artikel', 'kategori_artikel.kategori_artikel_id = artikel.kategori_artikel_id')->where('artikel.status', 'aktif')->where('artikel.kategori_artikel_id', $kategori_artikel_id)->get('artikel');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $query->row();
    }

}