<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manageweb extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->validate();
    }

    private function validate()
    {
        if($this->session->userdata('credit')['validated']){
            redirect('manage');
        }
    }

    public function index()
    {
        $this->login();
    }

    public function login()
    {
        // print_r(do_hash('kangcep'.UNIQPASS, 'md5'));die();
        $data['page']='Login '.TITLE;
        $this->load->view('tampil_login', $data);
    }

    public function go()
    {
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));
        $password = do_hash($password.UNIQPASS, 'md5');
        $this->db->where('rusernrame', $username);
        $this->db->where('prasswrord', $password);
        $query=$this->db->get('admin');

        if($query -> num_rows() == 1)
        {
            $row=$query->row();
            $sess_array['credit'] = array(
                'pk'=>$row->admin_id,
                'username'=>$row->rusernrame,
                'nama_lengkap'=>$row->nama_lengkap,
                'validated'=>TRUE
            );
            $this->session->set_userdata($sess_array);
            redirect('manage');
        } else {
            redirect('manageweb?cause=salah');
        }
    }
    
}
