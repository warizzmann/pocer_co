<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Read extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('artikel_m');
        $this->load->model('penulis_m');
    }

    public function r($id=null)
    {
        if ($id==null) {
            redirect('/');
        }
        $detail = $this->artikel_m->get_one_by($id, 'artikel_id');
        if (!$detail) {
            $err['page']='Halaman Tidak Ditemukan [404]';
            $err['page_id']='404';
            $this->load->view('atas',$err);
            $this->load->view('tampil_eror');
            $this->load->view('bawah');
        }else{
            $this->add_count($detail['url']);
            $data['page']=$detail['title'];
            $data['page_id']=$detail['url'];
            $data['artikel'] = $detail;
            $data['penulis'] = $this->penulis_m->get_detail($detail['username']);
            $data['popular'] = $this->artikel_m->get_limit(5, 'hit_count');
            $this->load->view('atas',$data);
            $this->load->view('tampil_baca');
            $this->load->view('bawah');
        }
    }

    public function index($url=null)
    {
        if ($url==null) {
            redirect('/');
        }
        $detail = $this->artikel_m->get_one_by($url, 'url');
        if (!$detail) {
            $err['page']='Halaman Tidak Ditemukan [404]';
            $err['page_id']='404';
            $this->load->view('atas',$err);
            $this->load->view('tampil_eror');
            $this->load->view('bawah');
        }else{
            $this->add_count($url);
            $data['page']=$detail['title'];
            $data['page_id']=$url;
            $data['artikel'] = $detail;
            $data['penulis'] = $this->penulis_m->get_detail($detail['username']);
            $data['popular'] = $this->artikel_m->get_limit(5, 'hit_count');
            $this->load->view('atas',$data);
            $this->load->view('tampil_baca');
            $this->load->view('bawah');
        }
    }

    function add_count($slug) {
        $this->load->helper('cookie');
        $check_visitor = $this->input->cookie($slug, FALSE);
        $ip = $this->input->ip_address();
        if ($check_visitor == false) {
            $cookie = array(
                "name" => $slug,
                "value" => "$ip",
                "expire" => time() + 7200,
                "secure" => false
            );
            $this->input->set_cookie($cookie);
            $count = $this->db->select('hit_count')->get_where('artikel', array('url' => $slug))->row();
            $this->db->where('url', $slug);
            $this->db->set('hit_count', ($count->hit_count + 1));
            $this->db->update('artikel');
        }
    }

}