<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('kategori_m');
        $this->load->model('artikel_m');
    }

    public function index($url_kategori=null)
    {
        if ($url_kategori==null) {
            redirect('/');
        }
        $kategori = $this->kategori_m->get_detail($url_kategori);
        if (!$kategori) {
            $err['page']='Halaman Tidak Ditemukan [404]';
            $err['page_id']='404';
            $this->load->view('atas',$err);
            $this->load->view('tampil_eror');
            $this->load->view('bawah');
        }else{
            $data['page']='Rubrik '.$kategori['nama_kategori'];
            $data['page_id']=$url_kategori;
            $data['artikel'] = $this->kategori_m->get_all_where($kategori['kategori_artikel_id']);
            $data['popular'] = $this->artikel_m->get_limit(5, 'hit_count');
            // print_r($data['artikel']);die();
            $this->load->view('atas',$data);
            $this->load->view('tampil_kategori');
            $this->load->view('bawah');
        }
    }

}
