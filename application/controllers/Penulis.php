<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penulis extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('penulis_m');
    }

    public function index()
    {
        $data['page']='Para Penulis';
        $data['page_id']='penulis';
        $data['penulis'] = $this->penulis_m->get_all();
        $this->load->view('atas',$data);
        $this->load->view('tampil_all_penulis');
        $this->load->view('bawah');
    }

    public function siapa($username=null)
    {
        if ($username==null) {
            redirect('/');
        }
        $penulis = $this->penulis_m->get_detail($username);
        if (!$penulis) {
            $err['page']='Halaman Tidak Ditemukan [404]';
            $err['page_id']='404';
            $this->load->view('atas',$err);
            $this->load->view('tampil_eror');
            $this->load->view('bawah');
        }else{
            $data['page']=$penulis['nama_lengkap'];
            $data['page_id']=$penulis['username'];
            $data['artikel'] = $this->penulis_m->get_artikel_penulis($penulis['penulis_id']);
            $data['penulis'] = $penulis;
            $this->load->view('atas',$data);
            $this->load->view('tampil_penulis');
            $this->load->view('bawah');
        }
    }

}
