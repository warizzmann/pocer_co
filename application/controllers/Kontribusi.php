<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontribusi extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['page']='Cara Berkontribusi';
        $data['page_id']='kontribusi';
        $this->load->view('atas',$data);
        $this->load->view('tampil_kontribusi');
        $this->load->view('bawah');
    }

}
