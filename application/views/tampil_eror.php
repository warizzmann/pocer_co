<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="center-block error-404 margin-bottom margin-top">
                <section class="text-center">
                <?php if ($page_id=='404') { ?>
                    <h1>404 <i class="fa fa-file"></i></h1>
                    <h2>Halaman Tidak Ditemukan</h2>
                    <p class="lead lead-lg">Mohon maaf, halaman yang anda akses tidak ditemukan.</p>
                <?php }else{ ?>
                    <h1>500 <i class="fa fa-bug"></i></h1>
                    <h2>SERVER ERROR</h2>
                    <p class="lead lead-lg">Mohon maaf, terjadi kesalahan pada halaman.</p>
                    <p><small style="font-size: 50%;"><?=$desc?></small></p>
                <?php } ?>
                </section>
            </div>
        </div>
    </div> <!-- row -->
</div> <!-- container -->