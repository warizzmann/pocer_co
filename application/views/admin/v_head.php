<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <link rel="shortcut icon" href="<?=base_url()?>assets/main/img/favicon-min.png" type="image/x-icon" />
    
    <title><?=$page?> - <?=TITLE?></title>
    
    <!-- BEGIN CORE FRAMEWORK -->
    <link href="<?=base_url()?>assets/admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- END CORE FRAMEWORK -->
    
    <!-- BEGIN THEME STYLES -->
    <link href="<?=base_url()?>assets/admin/plugins/datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/css/material.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/css/style.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/css/plugins.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/css/helpers.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/admin/css/responsive.css" rel="stylesheet" />
    <!-- END THEME STYLES -->
    
    <?php if(isset($css_files)){ foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; } ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-leftside fixed-header">
    <!-- BEGIN HEADER -->
    <header>
        <a href="<?=base_url()?>manage" class="logo bg-amber-800"><img src="<?=base_url()?>assets/main/img/logo.png" style="max-height:35px;max-width:35px;-webkit-filter: drop-shadow(0px 0px 2px #fff);filter:drop-shadow(0px 0px 2px #fff);" alt="<?=TITLE?>" /> <span style="font-size:smaller;"><?=TITLE?></span></a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="navbar-btn sidebar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-right visible-lg visible-md">
                <ul class="nav navbar-nav">                     
                    <li class="dropdown dropdown-box dropdown-tasks">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                          Halo, <strong><?=$this->session->userdata('credit')['nama_lengkap']?></strong>
                        </a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>" target="_blank">
                            <i class="fa fa-external-link"></i></span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END HEADER -->
         
    <div class="wrapper">
        <!-- BEGIN LEFTSIDE -->
        <div class="leftside bg-amber-900">
            <div class="sidebar">
                <!-- BEGIN NAV -->
                    <ul class="nav-sidebar">
                        <li <?=$page_id=='dashboard' ? 'class="active"':'' ?>>
                            <a href="<?=base_url()?>manage">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li <?=$page_id=='artikel' ? 'class="active"':'' ?>>
                            <a href="<?=base_url()?>manage/article">
                                <i class="fa fa-file"></i> <span>Artikel</span>
                            </a>
                        </li>
                        <li <?=$page_id=='featuredarticle' ? 'class="active"':'' ?>>
                            <a href="<?=base_url()?>manage/featuredarticle">
                                <i class="fa fa-star"></i> <span>Featured Artikel</span>
                            </a>
                        </li>
                        <li <?=$page_id=='penulis' ? 'class="active"':'' ?>>
                            <a href="<?=base_url()?>manage/penulis">
                                <i class="fa fa-street-view"></i> <span>Penulis</span>
                            </a>
                        </li>
                        <li <?=$page_id=='imagestorage' ? 'class="active"':'' ?>>
                            <a href="<?=base_url()?>manage/imagestorage">
                                <i class="fa fa-image"></i> <span>Image Storage</span>
                            </a>
                        </li>
                        <li <?=$page_id=='akun' ? 'class="active"':'' ?>>
                            <a href="<?=base_url()?>manage/akun">
                                <i class="fa fa-cogs"></i> <span>Akun</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?=base_url()?>home/signout" onclick="return confirm('Apakah anda ingin Logout?')">
                                <i class="fa fa-sign-out"></i> <span>Log Out</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END NAV -->
            </div><!-- /.sidebar -->
        </div>
        <!-- END LEFTSIDE -->
        
        <!-- BEGIN RIGHTSIDE -->
        <div class="rightside bg-grey-100">
                    
            <div class="container-fluid">