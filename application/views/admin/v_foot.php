<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
                <!-- BEGIN FOOTER -->
                <footer>
                    <div class="pull-left">
                        <span class="pull-left margin-right-15">&copy; 2016 <a href="<?=base_url()?>"><?=TITLE?></a> | <?=STITLE?></span>
                    </div>
                </footer>
                <!-- END FOOTER -->
                
            </div><!-- /.container-fluid -->
        </div><!-- /.rightside -->
    </div><!-- /.wrapper -->
    <!-- END CONTENT -->
        
    <!-- BEGIN JAVASCRIPTS -->
    
    <!-- BEGIN CORE PLUGINS -->
    <?php if(isset($js_files)){ foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
    <?php endforeach; }else{ ?>
    <script src="<?=base_url()?>assets/admin/plugins/jquery-1.11.1.min.js" type="text/javascript"></script>
    <?php } ?>
    <script src="<?=base_url()?>assets/admin/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/admin/plugins/bootstrap/js/holder.js"></script>
    <script src="<?=base_url()?>assets/admin/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/admin/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?=base_url()?>assets/admin/js/core.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    
    <!-- maniac -->
    <script src="<?=base_url()?>assets/admin/js/maniac.js" type="text/javascript"></script>
    
    <!-- dashboard -->
    <script type="text/javascript">
        $('#closeMe').click(function(e){    
            $('#noTif').fadeOut();
        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
</html>