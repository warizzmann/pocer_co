<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<!-- Mirrored from demo.yakuzi.eu/maniac/1.2/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Mar 2015 09:06:35 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	
	<title>Login Pocer</title>
	
	<!-- BEGIN CORE FRAMEWORK -->
	<link rel="shortcut icon" href="<?=base_url()?>assets/main/img/favicon.png" type="image/x-icon" />
	<link href="<?=base_url()?>assets/admin/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<!-- END CORE FRAMEWORK -->
	
	<!-- BEGIN PLUGIN STYLES -->
	<link href="<?=base_url()?>assets/admin/plugins/animate/animate.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/plugins/bootstrapValidator/bootstrapValidator.min.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/plugins/switchery/switchery.min.css" rel="stylesheet" />
	<!-- END PLUGIN STYLES -->
	
	<!-- BEGIN THEME STYLES -->
	<link href="<?=base_url()?>assets/admin/css/material.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/css/style.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/css/plugins.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/css/helpers.css" rel="stylesheet" />
	<link href="<?=base_url()?>assets/admin/css/responsive.css" rel="stylesheet" />
	<!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="auth-page height-auto">
	<!-- BEGIN CONTENT -->
	<div class="wrapper animated fadeInDown">
		<div class="panel overflow-hidden">
			<div class="<?php $ccc=$this->input->get('cause'); if($ccc=="belum"){ echo " bg-orange-500 ";}else if($ccc=="salah"){echo " bg-red-500 ";}else{echo"bg-light-blue-500";}?> padding-top-25 no-margin-bottom font-size-20 color-white text-center text-uppercase">
				<img src="<?=base_url()?>assets/main/img/logos/pojokcerpen_logo_long.png" style="max-width:90%;-webkit-filter: drop-shadow(0px 0px 2px #fff);filter:drop-shadow(0px 0px 2px #fff);" alt="<?=TITLE?>" />
			</div>
			<form id="checkform" method="post" action="<?=base_url()?>manageweb/go">
				<div class="alert <?php if($ccc=="belum"){ echo " bg-orange-500 ";}else if($ccc=="salah"){echo " bg-red-500 ";}else{echo"bg-light-blue-500";}?> text-center color-white no-radius no-margin padding-top-15 padding-bottom-30 padding-left-20 padding-right-20">
					<?php
						if($this->input->get('cause')=="belum"){
							echo "Anda belum Login.<br />Silahkan Login.";
						}else if($this->input->get('cause')=="salah"){
							echo "Password/Username yang anda masukkan salah.";
						}
					?>
				</div>
				<div class="box-body padding-md">				
					<div class="form-group">
						<input type="text" name="username" class="form-control input-lg" autofocus placeholder="Username"/>
					</div>
					
					<div class="form-group">
						<input type="password" name="password" class="form-control input-lg" placeholder="Password"/>
					</div>
					
					<button type="submit" class="btn btn-dark bg-light-green-500 padding-10 btn-block color-white"><i class="ion-log-in"></i> Masuk</button>
				</div>
			</form>
			<div class="panel-footer padding-md no-margin no-border bg-light-blue-500 text-center color-white">&copy; 2016 <a href="<?=base_url()?>"><?=TITLE?> | <?=STITLE?></a></div>
		</div>
	</div>
	<!-- END CONTENT -->
		
	<!-- BEGIN JAVASCRIPTS -->
	
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=base_url()?>assets/admin/plugins/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/admin/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/admin/plugins/bootstrap/js/holder.js"></script>
	<script src="<?=base_url()?>assets/admin/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>assets/admin/js/core.js" type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	
	<!-- bootstrap validator -->
	<script src="<?=base_url()?>assets/admin/plugins/bootstrapValidator/bootstrapValidator.min.js" type="text/javascript"></script>
	
	<!-- switchery -->
	<script src="<?=base_url()?>assets/admin/plugins/switchery/switchery.min.js" type="text/javascript"></script>
	
	<!-- maniac -->
	<script src="<?=base_url()?>assets/admin/js/maniac.js" type="text/javascript"></script>
	<script type="text/javascript">
		maniac.loadvalidator();
		maniac.loadswitchery();
	</script>
	
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

<!-- Mirrored from demo.yakuzi.eu/maniac/1.2/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Mar 2015 09:06:43 GMT -->
</html>