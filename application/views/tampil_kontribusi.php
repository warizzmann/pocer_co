<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container margin-top">

    <div class="row">

        <div class="col-md-12">
            <p><strong>Po</strong><strong>C</strong><strong>er.co</strong> menerima kontribusi tulisan untuk mengisi rubrik <strong>peristiwa literasi</strong>, <strong>sosok</strong>, <strong>resensi buku</strong>, <strong>info penerbit</strong>, dan <strong>cerita kawan</strong> dengan panjang tulisan antara 800 &ndash; 1200 kata, ditulis dengan font Cambria 12pt, spasi 1,5, rata kanan kiri, pada kertas ukuran A4.</p>
            <p><strong>Peristiwa literasi</strong> membahas berbagai peristiwa yang berkaitan dengan dunia perbukuan, misalnya event, reportase, diskusi, dan isu-isu terkait hal tersebut.</p>
            <p><strong>Sosok</strong> membahas tokoh-tokoh yang berpengaruh di dunia literasi, baik itu tokoh lokal maupun internasional.</p>
            <p><strong>Resensi buku</strong> membahas isi sebuah buku seputar sisi-sisi kelebihan dan kekurangan buku yang bersangkutan ataupun hal-hal lain seputar buku tersebut yang menarik. Buku yang diresensi adalah buku-buku yang diterbitkan oleh penerbit alternatif. Redaktur berhak mengurasi setiap resensi yang masuk. Naskah wajib menyertakan foto buku yang diresensi.&nbsp;</p>
            <p><strong>Jendela</strong><strong> penerbit</strong> membahas informasi seputar buku-buku terbaru yang akan diterbitkan oleh penerbit tertentu (khususnya alternatif).</p>
            <p><strong>Cerita kawan</strong> membahas pengalaman yang berkaitan dengan dunia buku, baik itu cerita perjalanan ke tempat-tempat buku menarik ataupun kisah mengoleksi buku-buku tertentu.</p>
            <p>Untuk kategori naskah <strong>peristiwa literasi, sosok, resensi buku</strong> dan <strong>cerita kawan</strong> yang dimuat akan mendapatkan kompensasi berupa fee dan voucher buku. Kompensasi tersebut akan dikirimkan selambat-lambatnya 2x24 jam setelah naskah dipublikasikan.&nbsp;&nbsp;</p>
            <p>Kontributor wajib mencantumkan biodata singkat, karya yang sudah terbit (kalau ada), akun sosmed, nomor yang bisa dihubungi, alamat domisili, dan nomor rekening bank lengkap. Biodata tersebut dicantumkan di bagian belakang naskah.&nbsp;</p>
            <p>Naskah tulisan dikirimkan ke <strong>redaksi@pocer.co</strong> dalam format docx dengan subjek <strong>naskah + nama rubrik</strong>. kontributor yang naskahnya layak muat akan dihubungi oleh redaksi.</p>
            <p>Jika sampai <strong>se</strong><strong>puluh hari</strong> dari waktu pengiriman, tulisan tidak dimuat, maka kontributor berhak mengirimkan tulisan tersebut ke media lain.</p>
            <p>Redaksi berhak mengedit tulisan dan gambar sebelum dipublikasikan.</p>
        </div>

    </div>

</div> <!-- container -->